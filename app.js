var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var cors = require('cors');
config = require('config');
var moment = require('moment');
var seller = require('./routes/seller');
var qs = require('querystring')
var admin = require('./routes/admin');

// files to be require for apis's
var users = require('./routes/users.js');  // users

md5 = require('md5');
var app = express();


process.env.NODE_ENV = 'development';

dbconnection = require('./routes/connection.js');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({ limit: "50mb",extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(express.static(__dirname + '/public'));

app.use(cors());



app.get('/', function (req, res) {
    res.render('local_test');
});
app.get('/admin', function (req, res) {
    res.render('local_test1');
});

app.get('/category', function (req, res) {
    res.render('categoryAdd');
});

app.get('/product', function (req, res) {
    res.render('productAdd');
});

app.put('/met', function (req, res) {

    console.log(req);
   // res.type('json');
  //  res.jsonp(req.query.bh);
});
// users API

app.post('/sign_up',seller.register);


app.post('/addcategory',admin.addCategory);
app.post('/adminLogin',admin.login);
app.post('/login',seller.login);

var server = http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});
//gotigy



