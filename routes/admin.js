/**asd
 * Created by ankit kumar on 7/7/16.
 */

var async = require('async');
var sendResponse = require('./sendResponse.js');
var md5 = require('md5');
var jwt = require('jsonwebtoken');
var commonfunction = require('./commonfunction.js');
var validator = require('validator');
const saltRounds = 10;

/*
 * ===============
 *  sign up
* * */
exports.register = function (request, reply) {
    var email;
    var name;
    var password;
    var dob;
    var maritalStatus;
    var streetAddress;
    var city;
    var state;
    var country;
    var is_stylist = 0; // 1: stylist , 0 : users
    var accessToken ;
    var deviceTokenAndroid=0;
    var deviceTokenIos=0;

    async.auto({
        checkBlank: function (callback) {

            if (!request.body.email) {
                var msg = "Please enter email or enter valid email";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if(validator.isEmail(request.body.email)==false)
            {
                var msg = "Please enter valid email";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.name) {
                var msg = "Please enter name ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.username) {
                var msg = "Please enter username ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.password) {
                var msg = "Please enter password ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.dob) {
                var msg = "Please enter dob ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.marital_status) {
                var msg = "Please enter marital status ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.street_address) {
                var msg = "Please enter street address ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.city) {
                var msg = "Please enter city ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.state) {
                var msg = "Please enter state ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.country) {
                var msg = "Please enter state ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if ((!request.body.device_token_android) && (!request.body.device_token_ios)) {
                var msg = "Please send device token ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (request.body.is_stylist == 1)
                is_stylist = 1;

            email = request.body.email;
            name = request.body.name;
            password = request.body.password;
            username = request.body.username;
            dob = request.body.dob;
            maritalStatus = request.body.marital_status;
            streetAddress = request.body.street_address;
            city = request.body.city;
            state = request.body.state;
            country = request.body.country;
            deviceTokenAndroid=request.body.device_token_android;
            deviceTokenIos=request.body.device_token_ios;
            callback(null,"done");
        },
        checkUserExist: ['checkBlank', function (callback, result) {
            console.log(result);
            var sql = " select * from users where email= ? "
            connection.query(sql, [email], function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (result.length > 0) {
                        var msg = " email already exist";
                        sendResponse.sendErrorMessage(msg, reply, 500);
                    }
                    else {
                        callback(null);
                    }
                }
            })
        }],
        checkUsername : ['checkUserExist',function(callback,result)
        {
            console.log("....checkUsername.............")
            var sql = " select * from users where username = ? "
            connection.query(sql, [username], function (err, result) {
                if (err)
                {   console.log(err);
                    callback(err);
                }
                else {
                    if (result.length > 0) {
                        var msg = " username already exist";
                        sendResponse.sendErrorMessage(msg, reply, 500);
                    }
                    else {
                        callback(null);
                    }
                }
            })

        }],


        storeData: ['checkUsername', function (callback, result) {
            var sql = " insert into users(email,name,access_token,password,username,dob,street_address,marital_status,is_stylist,city,state,country,device_token_android,device_token_ios) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
            connection.query(sql, [email, name,Math.random(), password, username, dob, streetAddress, maritalStatus, is_stylist, city, state, country,deviceTokenAndroid,deviceTokenIos], function (err, result) {
                if (err) {
                    console.log(err);
                    callback(err);
                }
                else {
                    callback(null);
                    console.log(result);
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            var msg = err.toString();
            sendResponse.sendErrorMessage(msg, reply, 500);
        }
        else
        {
            var msg="Signup successful!" ;
            sendResponse.sendSuccessMessage(msg,reply,200);
        }
    })

}


exports.login = function (request, reply) {
    var email;
    var password;


    async.waterfall([
        function (callback) {
            console.log("seller login");
            if (!request.body.email) {
                var msg = "Please enter email or enter valid email";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }

            if (!request.body.password) {
                var msg = "Please enter password ";

            }

            email = request.body.email;
            password = request.body.password;
            console.log(email+" "+password);
            callback(null, "no missing field");
        },

        function (res, callback) {
            console.log(res);
            var sql = " select * from admin where email= ? and password = ?";
            connection.query(sql, [email, password], function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (result.length > 0) {
                        console.log(result[0].name);
                        callback(null,request._startTime)

                        sql = "UPDATE `admin` SET `accessToken`= ? WHERE email = ? AND password= ? ";
                        connection.query(sql,[Math.random(),email,password],function (err,resu) {
                            if(err)
                                callback(err);
                            else{

                                console.log("works");

                            }
                        });
                    }
                    else {
                        var msg = "no such user exists";
                        callback(null, "invalid user");
                    }
                }
            })
        }

    ], function (err, result) {
        if (err) {
            var msg = err.toString();
            msg += "sent by call back";
            sendResponse.sendErrorMessage(msg, reply, 500);
        }
        else
        {
            var msg=result.toString() ;
            sendResponse.sendSuccessMessage(msg,reply,200);
        }
    });

}



/*
* admin add category functions
* */
exports.addCategory = function (request, reply) {

 var name;
 var description;
 var active;

    async.waterfall([
        function(callback){
            if(!request.body.name){
                var msg = "no body name";
                callback(msg);
            }
            if(!request.body.description){
                var msg = "no description";
                callback(msg);
            }
            if(!request.body.active){
                var msg = "enter the state of category";
                callback(msg);
            }

            name = request.body.name;
            description = request.body.description;
            active = request.body.active;
            callback(null);
        },
        function (callback) {
            var queryy = "select * from category where name = ?";
            connection.query(queryy, [name], function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (result.length > 0)
                        callback("already existing category");

                    else {
                        var queryy = "insert into category (name, description, active) values(?,?,?)";
                        connection.query(queryy,[name,description,active],function (err, result) {

                           if (err)
                           {
                               callback(err);
                           }
                           else
                           {
                               callback(null,"category "+name+" added successfully");
                           }
                        });

                    }
                }
            });

        },


    ],function (err, result) {
        if (err){

            var msg = err.toString();
            sendResponse.sendErrorMessage(msg, reply, 500);
        }
        else{

            var msg = result.toString();
            sendResponse.sendSuccessMessage(msg,reply, 200);
        }

    });
    
}