/**
 * Created by ankit kumar on 7/7/16.
 */

var async = require('async');
var sendResponse = require('./sendResponse.js');
var md5 = require('md5');
var jwt = require('jsonwebtoken');
var commonfunction = require('./commonfunction.js');
var validator = require('validator');
const saltRounds = 10;




/*
 * ===============
 *  sign up
 * */
exports.register = function (request, reply) {
    var email;
    var name;
    var password;

    var address;
    var city;
    var contact;
    var accessToken ;
    var latitude;
    var longitude;

    async.waterfall([
        function (callback) {

            if (!request.body.id) {
                var msg = "Please enter email or enter valid email";
                return sendResponse.parameterMissingError(msg,  400, reply);
            }
            if(validator.isEmail(request.body.id)==false)
            {
                var msg = "Please enter valid email";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.name) {
                var msg = "Please enter name ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.address) {
                var msg = "Please enter address ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.password) {
                var msg = "Please enter password ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.city) {
                var msg = "Please enter city ";
                return sendResponse.parameterMissingError(msg, 400, reply);
            }
            if (!request.body.contact) {
                var msg = "Please enter marital status ";
                return  sendResponse.parameterMissingError(msg, 400, reply);
            }




            email = request.body.id;
            name = request.body.name;
            password = request.body.password;
            contact = request.body.contact;
            city = request.body.city;
            address = request.body.address;
            callback (null);

        },
       function (callback) {
            var sql = " select * from supplier where email= ? "
            connection.query(sql, [email], function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (result.length > 0) {
                        var msg = " email already exist";
                        sendResponse.sendErrorMessage(msg, reply, 500);
                    }
                    else {
                        callback(null);
                    }
                }
            })
        },

    function (callback) {
            var sql = " insert into supplier(name,password,email,contact,lastLogin,accessToken,address,city,latitude,longitude) values(?,?,?,?,?,?,?,?,?,?) ";
            connection.query(sql, [name,password, email, contact, "0", Math.random(), address, city, 0,0], function (err, result) {
                if (err) {
                    console.log(err);
                    callback(err);
                }
                else {
                   callback(null);
                }
            })
        }
    ], function (err, result) {
        if (err) {
            var msg = err.toString();
            sendResponse.sendErrorMessage(msg, reply, 500);
        }
        else
        {
            var msg="Signup successful!" ;

            sendResponse.sendSuccessMessage(msg,reply,200);
        }
    })

}


exports.login = function (request, reply) {
    var email;
    var password;

    async.waterfall([
        function (callback) {

            if (!request.body.email) {
                var msg = "Please enter email or enter valid email";
                callback(msg);
            }else{
                email = request.body.email;
            }

            if (!request.body.password) {
                var msg = "Please enter password ";
                callback(msg);
            }else{
                password = request.body.password;
            }



            callback(null);
        },

        function (callback) {
            //console.log(resut);
            var sql = " select * from supplier where email= ? and password = ?"
            connection.query(sql, [email, password], function (err, result) {
                if (err)
                    callback(err);
                else {
                    if (result.length > 0) {
                        console.log(result[0].name);


                        sql = "UPDATE `supplier` SET `accessToken`= ? WHERE email = ? AND password= ? ";
                        connection.query(sql,[Math.random(),email,password],function (err,result) {
                            if(err)
                                callback(err);
                            else{
                                callback(null,"logged in");
                            }
                        });
                    }
                    else {
                        var msg = "no such user exists";
                        callback(null,msg);
                    }
                }
            })
        }








    ], function (err, result) {
        if (err) {
            sendResponse.sendErrorMessage(err, reply, 500);
        }
        else
        {
            var msg=result.toString() ;
            sendResponse.sendSuccessMessage(msg,reply,200);
        }
    });

}
